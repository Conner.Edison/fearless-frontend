function createCard(name, description, pictureUrl, dates, location) {
  return `
    <div class="card shadow p-2 ms-2 bg-body rounded">
      <img src=${pictureUrl} class="card-img-top">
      <div class="card-body">
        <h5 class="card-title text-center">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted text-center">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">${dates}</div>
    </div>
    <br>
      `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.log("bad response");
    } else {
      const data = await response.json();
      for (const conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toDateString();
          const endDate = new Date(details.conference.ends).toDateString();
          const dates = startDate + " - " + endDate
          const location = details.conference.location.name
          const html = createCard(title, description, pictureUrl, dates, location);
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }

    }
  } catch (error) {
    console.error(error);
    console.log(error);
  }

});